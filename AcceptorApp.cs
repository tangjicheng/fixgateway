using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using QuickFix;
using QuickFix.Fields;

namespace FixGateway
{
    public class AcceptorApp : /*QuickFix.MessageCracker,*/ QuickFix.IApplication
    {
        private static string FormatMessage(string direction, Message message, SessionID sessionID)
        {
            return $"{direction}: {sessionID} msg: {message.ToString().Replace('\x01', '|')}";
        }
        #region QuickFix.Application Methods


        public void FromApp(Message message, SessionID sessionID)
        {
            Console.WriteLine(FormatMessage("IN", message, sessionID));
        }

        public void ToApp(Message message, SessionID sessionID)
        {
            Console.WriteLine(FormatMessage("OUT", message, sessionID));
        }

        public void FromAdmin(Message message, SessionID sessionID)
        {
            Console.WriteLine(FormatMessage("IN", message, sessionID));
        }

        public void ToAdmin(Message message, SessionID sessionID)
        {
            Console.WriteLine(FormatMessage("OUT", message, sessionID));
        }

        public void OnCreate(SessionID sessionID)
        {
            Console.WriteLine($"OnCreate: {sessionID}");
        }

        public void OnLogout(SessionID sessionID)
        {
            Console.WriteLine($"OnLogout: {sessionID}");
        }

        public void OnLogon(SessionID sessionID)
        {
            Console.WriteLine($"OnLogon: {sessionID}");
        }
        #endregion
    }
}